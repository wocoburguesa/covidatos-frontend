# Covidatos

Este es el repo del frontend de covidatos. Debería bastar con `npm install` y `npm start`

El sitio live de covidatos está [acá](https://covidatos.pe/), junto con más información de agradecimientos y uso.

Para cualquier duda pueden encontrarme en Twitter como @covidatos o en el mail covidatos.peru@gmail.com.

2020 - Marco Flores