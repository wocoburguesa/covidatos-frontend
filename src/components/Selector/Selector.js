import React, { useState } from 'react'
import styled from 'styled-components'
import { Segment, Dimmer, Loader, Input, Label } from 'semantic-ui-react'
import { Dropdown, Button } from 'semantic-ui-react'
import { metricOptions, chartOptions } from './constants'

const StyledSegmentContainer = styled.div`
    text-align: center;
`

const StyledSegment = styled(Segment)`

    :first-child {
        padding-bottom: 0;
        margin: 0 0 1px 0;
    }

    :nth-child(2) {
        padding-top: 0;
        padding-bottom: 0;
        margin: 1px 0;
    }

    :nth-child(3) {
        padding-top: 0;
        margin: 1px 0 0 0;
    }
`

const StyledDropdown = styled(Dropdown)`
    font-family: 'Bitter',serif;
`

const StyledButton = styled(Button)`
    font-family: 'Bitter',serif !important;
    margin-top: 20px !important;
    font-size: 16px !important;
`

const StyledLoader = styled(Loader)`
  font-family: 'Bitter', serif;
`

const StyledDimmer = styled(Dimmer)`
  background-color: #1f1c1c !important;
`

const MetricSelectionContainer = styled.div``

const DateInput = styled(Input)``

const DateInputContainer = styled.div`
    width: 50%;
    text-align: left;
    display: inline-block;

    .label {
        background-color: rgba(0,0,0,0);
        color: #fafafa;
    }
`

export const Selector = ({
    handleAdd,
    showLoading,
    zonesMap,
    showMetricOptions,
    handleUpdateMetric,
    handleUpdateChartType,
    datesDomainStart,
    datesDomainEnd,
    handleUpdateDateStart = () => {},
    handleUpdateDateEnd = () => {},
    showAddButton = true,
}) => {
    const [departamento, setDepartamento] = useState('')
    const [provincia, setProvincia] = useState('')
    const [distrito, setDistrito] = useState('')
    const [metric, setMetric] = useState('cumulative_value')

    const [provinciaOptions, setProvinciaOptions] = useState()
    const [distritoOptions, setDistritoOptions] = useState()

    const [distritoPlaceholder, setDistritoPlaceholder] = useState('Distrito')

    const [dateStart, setDateStart] = useState(datesDomainStart)
    const [dateEnd, setDateEnd] = useState(datesDomainEnd)

    const departamentoOptions = Object.keys(zonesMap).sort((keyA, keyB) => {
        if (keyA < keyB) {
            return -1
        } else {
            return 0
        }
    }).map((key) => {
        return {
            key: key,
            value: key,
            text: key
        }
    })

    const handleChangeDepartamento = (e, { value }) => {
        setDepartamento(value)
        setProvinciaOptions(
            Object.keys(zonesMap[value]).sort((keyA, keyB) => {
                if (keyA < keyB) {
                    return -1
                } else {
                    return 0
                }
            }).map((key) => {
                return {
                    key: key,
                    value: key,
                    text: key
                }
            })
        )

        setProvincia('')
        setDistrito('')
    }

    const handleChangeProvincia = (e, { value }) => {
        setProvincia(value)
        setDistritoOptions(
            Object.keys(zonesMap[departamento][value]).sort((keyA, keyB) => {
                if (keyA < keyB) {
                    return -1
                } else {
                    return 0
                }
            }).map((key) => {
                return {
                    key: key,
                    value: key,
                    text: key
                }
            })
        )

        setDistrito('')
    }

    const handleChangeDistrito = (e, { value }) => {
        setDistrito(value)
    }

    const handleChangeMetric = (e, { value }) => {
        setMetric(value)
        handleUpdateMetric(value)
    }

    const handleChangeChartType = (e, { value }) => {
        // setChartType(value)
        handleUpdateChartType(value)
    }

    const handleChangeDateStart = (e, { value }) => {
        setDateStart(value)
        handleUpdateDateStart(value)
    }

    const handleChangeDateEnd = (e, { value }) => {
        setDateEnd(value)
        handleUpdateDateEnd(value)
    }

    const handleClick = (e) => {
        handleAdd({ departamento, provincia, distrito, newMetric: metric })
        setDistrito('')
        setDistritoPlaceholder('Compara con otro distrito')
    }

    return (
        <StyledSegmentContainer>
            <StyledSegment inverted>
                <StyledDropdown
                    inverted
                    onChange={handleChangeDepartamento}
                    clearable
                    fluid
                    search
                    selection
                    options={departamentoOptions}
                    placeholder='Departamento'
                />
            </StyledSegment>
            { departamento && <StyledSegment inverted>
                <StyledDropdown
                    inverted
                    onChange={handleChangeProvincia}
                    clearable
                    fluid
                    search
                    selection
                    options={provinciaOptions}
                    placeholder='Provincia'
                />
            </StyledSegment>}
            { provincia && <StyledSegment inverted>
                <StyledDropdown
                    inverted
                    onChange={handleChangeDistrito}
                    clearable
                    fluid
                    search
                    selection
                    value={distrito}
                    options={distritoOptions}
                    placeholder={distritoPlaceholder}
                />
            </StyledSegment>}
            { (showMetricOptions || distrito) && <StyledSegment inverted>
                <StyledDropdown
                    inverted
                    onChange={handleChangeMetric}
                    clearable
                    fluid
                    search
                    selection
                    defaultValue={'cumulative_value'}
                    options={metricOptions}
                    placeholder='Casos Totales'
                />
                <StyledDropdown
                    inverted
                    onChange={handleChangeChartType}
                    clearable
                    fluid
                    search
                    selection
                    defaultValue={'line'}
                    options={chartOptions}
                    placeholder='Tipo de gráfico'
                />
                <DateInputContainer>
                    <Label inverted>Inicio</Label>
                    <DateInput inverted type="date" onChange={handleChangeDateStart} value={dateStart} />
                </DateInputContainer>

                <DateInputContainer>
                    <Label inverted>Fin</Label>
                    <DateInput inverted type="date" onChange={handleChangeDateEnd} value={dateEnd} />
                </DateInputContainer>

            </StyledSegment>}
            { showAddButton ? (
                <StyledButton inverted onClick={handleClick} disabled={
                    !(departamento && provincia && distrito) || showLoading
                }>
                    { showLoading ? (
                        <StyledDimmer active>
                            <StyledLoader inverted>Cargando</StyledLoader>
                        </StyledDimmer>
                    ) : (
                        'Agregar'
                    )}
                </StyledButton>
            ) : null }
        </StyledSegmentContainer>
    )
}