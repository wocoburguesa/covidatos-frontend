export const metricOptions = [
    {
        key: 'cumulative_value',
        value: 'cumulative_value',
        text: 'Casos totales'
    },
    {
        key: 'r_t',
        value: 'r_t',
        text: 'R'
    },
    {
        key: 'cumulative_pct_total',
        value: 'cumulative_pct_total',
        text: '% de población'
    },
    {
        key: 'cumulative_100k_total',
        value: 'cumulative_100k_total',
        text: 'Casos por 100k habitantes'
    },
    {
        key: 'daily_value',
        value: 'daily_value',
        text: 'Casos diarios'
    },
    {
        key: 'cumulative_0_5',
        value: 'cumulative_0_5',
        text: 'Casos totales de 0 a 5 años'
    },
    {
        key: 'cumulative_6_18',
        value: 'cumulative_6_18',
        text: 'Casos totales de 6 a 18 años'
    },
    {
        key: 'cumulative_19_24',
        value: 'cumulative_19_24',
        text: 'Casos totales de 19 a 24 años'
    },
    {
        key: 'cumulative_25_39',
        value: 'cumulative_25_39',
        text: 'Casos totales de 25 a 39 años'
    },
    {
        key: 'cumulative_40_64',
        value: 'cumulative_40_64',
        text: 'Casos totales de 40 a 64 años'
    },
    {
        key: 'cumulative_over_65',
        value: 'cumulative_over_65',
        text: 'Casos totales de 65 años a más'
    },
    {
        key: 'cumulative_no_age',
        value: 'cumulative_no_age',
        text: 'Casos totales sin edad registrada'
    },
    {
        key: 'cumulative_pct_0_5',
        value: 'cumulative_pct_0_5',
        text: '% de población 0-5 años'
    },
    {
        key: 'cumulative_pct_6_18',
        value: 'cumulative_pct_6_18',
        text: '% de población 6-18 años'
    },
    {
        key: 'cumulative_pct_19_24',
        value: 'cumulative_pct_19_24',
        text: '% de población 19-24 años'
    },
    {
        key: 'cumulative_pct_25_39',
        value: 'cumulative_pct_25_39',
        text: '% de población 25-39 años'
    },
    {
        key: 'cumulative_pct_40_64',
        value: 'cumulative_pct_40_64',
        text: '% de población 40-64 años'
    },
    {
        key: 'cumulative_pct_over_65',
        value: 'cumulative_pct_over_65',
        text: '% de población mayor a 65 años'
    },
    {
        key: 'cumulative_100k_0_5',
        value: 'cumulative_100k_0_5',
        text: 'Casos por 100k habitantes: 0-5 años'
    },
    {
        key: 'cumulative_100k_6_18',
        value: 'cumulative_100k_6_18',
        text: 'Casos por 100k habitantes: 6-18 años'
    },
    {
        key: 'cumulative_100k_19_24',
        value: 'cumulative_100k_19_24',
        text: 'Casos por 100k habitantes: 19-24 años'
    },
    {
        key: 'cumulative_100k_25_39',
        value: 'cumulative_100k_25_39',
        text: 'Casos por 100k habitantes: 25-39 años'
    },
    {
        key: 'cumulative_100k_40_64',
        value: 'cumulative_100k_40_64',
        text: 'Casos por 100k habitantes: 40-64 años'
    },
    {
        key: 'cumulative_100k_over_65',
        value: 'cumulative_100k_over_65',
        text: 'Casos por 100k habitantes: mayor a 65 años'
    },
    {
        key: 'daily_0_5',
        value: 'daily_0_5',
        text: 'Casos diarios de 0 a 5 años'
    },
    {
        key: 'daily_6_18',
        value: 'daily_6_18',
        text: 'Casos diarios de 6 a 18 años'
    },
    {
        key: 'daily_19_24',
        value: 'daily_19_24',
        text: 'Casos diarios de 19 a 24 años'
    },
    {
        key: 'daily_25_39',
        value: 'daily_25_39',
        text: 'Casos diarios de 25 a 39 años'
    },
    {
        key: 'daily_40_64',
        value: 'daily_40_64',
        text: 'Casos diarios de 40 a 64 años'
    },
    {
        key: 'daily_over_65',
        value: 'daily_over_65',
        text: 'Casos diarios de 65 años a más'
    },
    {
        key: 'daily_no_age',
        value: 'daily_no_age',
        text: 'Casos diarios sin edad registrada'
    },
    // {
    //     key: 'growth_rate',
    //     value: 'growth_rate',
    //     text: 'Incremento'
    // },
    // {
    //     key: 'acceleration',
    //     value: 'acceleration',
    //     text: 'Aceleración'
    // }
]

export const chartOptions = [
    {
        key: 'line',
        value: 'line',
        text: 'Línea'
    },
    {
        key: 'bar',
        value: 'bar',
        text: 'Barra'
    },
    {
        key: 'area',
        value: 'area',
        text: 'Área'
    },
    // {
    //     key: 'pie',
    //     value: 'pie',
    //     text: 'Radial (Pie Chart)'
    // }
]