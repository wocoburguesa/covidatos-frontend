import React, { useState, useEffect } from 'react'
import {
  Container,
  Header,
  Button,
  Icon
} from 'semantic-ui-react'
import axios from 'axios'
import styled from 'styled-components'
import { Dashboard } from '../Dashboard'

console.log("Sapo?")

Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

// https://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
         ].join('-');
};

const StyledContainer = styled(Container)`
  background-color: #1f1c1c;

  p {
    font-family: 'Bitter', serif;
    color: #fafafa;
    font-size: 16px;
  }

  .hide-info {
    font-family: 'Bitter', serif;
    cursor: pointer;
    color: #fafafa;
    text-decoration: underline;
    font-weight: 600;
    font-size: 12px;
  }

  @media (max-width: 480px) {
    p {
      font-size: 14px;
    }
  }
`

const StyledHeader = styled(Header)`
  font-family: 'Bitter', serif;
  color: #fafafa;
  font-size: 4rem;
  text-align: center;

  @media (max-width: 480px) {
    font-size: 2rem;
    margin-bottom: 25px;
  }
`


const StyledFooter = styled.div`
  margin: 150px auto 20px auto;
  bottom: 0;
  text-align: center;
  color: #fafafa;
  font-family: 'Bitter', serif;
`

const InfoContainer = styled.div`
  margin-top: 50px;
  margin-bottom: 50px;

  @media (max-width: 480px) {
    margin-bottom: 20px;
  }
`

const InfoButtonContainer = styled.div`
  text-align: center;
  margin-bottom: 40px;

  button {
    font-size: 8px;
  }
`

const Home = props => {
  const [zonesMap, setZonesMap] = useState()
  const [showInfo, setShowInfo] = useState(true)

  useEffect(() => {
    axios.get('https://covidatos-regiones.s3-sa-east-1.amazonaws.com/zones_map.json')
      .then(res => {
        setZonesMap(res.data)
      })
  }, [])

  const handleHideInfo = () => {
    setShowInfo(false)
  }

  const handleShowInfo = () => {
    setShowInfo(true)
  }

  return (
    <>
      <StyledContainer style={{ marginTop: '7em' }}>
        <StyledHeader as='h1'>Datos a nivel distrital en Perú</StyledHeader>
        <InfoButtonContainer hidden={showInfo}>
          <Button onClick={handleShowInfo} circular inverted icon="info" />
        </InfoButtonContainer>

        <InfoContainer hidden={!showInfo}>
          <p>Con esta herramienta podrás comparar los datos de casos de COVID-19 confirmados a nivel distrital. La intención es ofrecer visibilidad más detallada para poder tomar decisiones focalizadas.</p>

          <p><Icon inverted name="hand point right outline"></Icon> Haz clic en el + y empieza a <strong>crear</strong> visualizaciones combinando distritos e indicadores</p>
          <p><Icon inverted name="hand point right outline"></Icon> Puedes <strong>agregar</strong> cuantas visualizaciones desees.</p>
          <p><Icon inverted name="hand point right outline"></Icon> Si deseas <strong>compartir</strong> tu tablero puedes copiar y enviar la URL, cambia a medida que usas la aplicación.</p>

          <p>Todos los datos provienen del portal de datos abiertos publicado recientemente por el MINSA. Todas las deficiencias en los datos también. Háganselas saber en <a href="https://twitter.com/Minsa_Peru" target="_blank">@MINSA</a>.</p>

          <p>Los bugs sí son todos míos. Háganmelos saber en <a href="https://twitter.com/covidatos" target="_blank">@covidatos</a> o a covidatos.peru@gmail.com.</p>

          <p>Recuerda que en las visualizaciones de R sólo podrás agregar un distrito por gráfico. Esto es para evitar la saturación visual. Claro que puedes agregar todos los gráficos que quieras con R individuales. Agradezco a Ragi Burhum (<a href="https://twitter.com/rburhum" target="_blank">@rburhum</a>) e, indirectamente, a Kevin Systrom por publicar sus algoritmos para calcular R.</p>

          <p><strong>*Actualizado el 24 de julio a las 18:41.</strong></p>
          <a className={'hide-info'} onClick={handleHideInfo}>Ocultar esta descripción</a>
        </InfoContainer>

        <Dashboard zonesMap={zonesMap}></Dashboard>

        <StyledFooter><span>Hecho en Perú 🇵🇪</span></StyledFooter>
      </StyledContainer>
    </>
  )
}

export default Home