import React, { useState } from 'react'
import styled from 'styled-components'
import { Grid, Button, Icon, Modal, Loader, Dimmer, List, Input } from 'semantic-ui-react'

import { Selector } from '../Selector'
import { Visualizer } from '../Visualizer'

import { colors } from './constants'
import { getMetricTextName } from '../../utils'
import { metricOptions } from '../Selector/constants'

export const StyledModalContent = styled(Modal.Content)`
    background-color: #1f1c1c !important;
    color: #fafafa;
    font-family: 'Bitter',serif;
`

const StyledHintContainer = styled.div`
    color: #fafafa;

    i {
        font-size: 25px;
    }

    p {
        font-size: 16px;
        font-family: 'Bitter',serif
    }

    .hand.point.up.outline {
        display: none;
    }

    @media (max-width: 480px) {
        text-align: center;

        p {
            margin-top: 10px;
        }

        .hand.point.left.outline {
            display: none;
        }

        .hand.point.up.outline {
            display: inline-block;
        }
    }

    @media (max-width: 767px) {
        text-align: center;

        .hand.point.left.outline {
            display: none;
        }

        .hand.point.up.outline {
            display: inline-block;
        }
    }
`

const StyledLoader = styled(Loader)`
  font-family: 'Bitter', serif;
`

const StyledDimmer = styled(Dimmer)`
  background-color: #1f1c1c !important;
`

export const StyledModalActions = styled(Modal.Actions)`
    background-color: #1f1c1c !important;
    color: #fafafa;
    font-family: 'Bitter',serif;
`

const StyledAddVisButton = styled(Button)`
    font-size: 15px;
    font-family: 'Bitter',serif;
`

const StyledSpan = styled.span`
  font-size: 18px;
  font-family: 'Bitter',serif;
`

const ListItem = styled(List.Item)`
    position: relative;
`

const ColorPickerTrigger = styled(Button)`
    height: 36px;
    width: 36px;
    vertical-align: bottom;
`

const ColorGrid = styled(Grid)`
    border: 1px solid #fafafa;
    padding: 2px !important;
    border-radius: 3px;
    background-color: #3a3a3a;
`

const ColorPickerContainer = styled.div`
    position: absolute;
    top: 60px;
    right: 0;
    width: 116px;
    z-index: 999;
`

const ColorOption = styled(Grid.Column)`
    padding: 2px !important;
`

const ColorOptionInner = styled.div`
    height: 31.5px;
    border-radius: 2px;
`

const TitleInputContainer = styled.div`
    text-align: center;
    margin: 10px 0;
`

const TitleInput = styled(Input)`
    border-bottom: 1px solid #ddd;

    input {
        font-family: 'Bitter', serif;
        font-size: 18px
        text-align: center;
    }
`

export const AddVisModal = ({
    zonesMap,
    trigger,
    open,
    onClose,
    handleAddVis
}) => {
    const [loading, setLoading] = useState(false)
    const [metric, setMetric] = useState('cumulative_value')
    const [chartType, setChartType] = useState('line')
    const [title, setTitle] = useState('Casos totales')
    const [dataPoints, setDataPoints] = useState([])

    const [datesDomainStart, setDatesDomainStart] = useState()
    const [datesDomainEnd, setDatesDomainEnd] = useState()

    const resetValues = () => {
        setLoading(false)
        setMetric('cumulative_value')
        setChartType('line')
        setTitle('Casos totales')
        setDataPoints([])
        setDatesDomainStart(null)
        setDatesDomainEnd(null)
    }

    const handleAdd = ({ departamento, provincia, distrito, newMetric }) => {
        // setLoading(true)
        setMetric(newMetric)

        const found = dataPoints.filter(dataPoint => {
            return (
                dataPoint.departamento === departamento &&
                dataPoint.provincia === provincia &&
                dataPoint.distrito === distrito
            )
        })

        if (!!found.length) return

        setDataPoints(dataPoints => [
            ...dataPoints,
            {
                distrito: distrito,
                provincia: provincia,
                departamento: departamento,
                color: colors[Math.floor(Math.random() * (colors.length))]
            }
        ])
    }

    const handleChangeColor = (clickedDataPoint, color) => {
        const updated = dataPoints.map(dataPoint => {
            if (
                dataPoint.departamento === clickedDataPoint.departamento &&
                dataPoint.provincia === clickedDataPoint.provincia &&
                dataPoint.distrito === clickedDataPoint.distrito
            ) {
                // highlight current one or something
                return {
                    ...dataPoint,
                    color: color,
                    showColorPicker: false
                }
            } else {
                return dataPoint
            }
        })

        setDataPoints(updated)
    }

    const toggleColorPicker = (clickedDataPoint) => {
        const updated = dataPoints.map(dataPoint => {
            if (
                dataPoint.departamento === clickedDataPoint.departamento &&
                dataPoint.provincia === clickedDataPoint.provincia &&
                dataPoint.distrito === clickedDataPoint.distrito
            ) {
                // highlight current one or something
                return {
                    ...dataPoint,
                    showColorPicker: !dataPoint.showColorPicker
                }
            } else {
                return dataPoint
            }
        })

        setDataPoints(updated)
    }

    const handleRemove = (toDelete) => {
        setDataPoints((dataPoints) => {
            return dataPoints.filter(item => (item.distrito !== toDelete.distrito))
       })
    }

    const handleUpdateMetric = (metric) => {
        setMetric(metric)
        setTitle(getMetricTextName(metric, metricOptions))
    }

    const handleUpdateChartType = (type) => {
        setChartType(type)
    }

    const handleUpdateTitle = (e, { value }) => {
        setTitle(value)
    }

    const handleUpdateDatesDomain = (start, end) => {
        setDatesDomainStart(new Date(start))
        setDatesDomainEnd(new Date(end))
    }

    const handleUpdateDateStart = (date) => {
        setDatesDomainStart(date)
    }

    const handleUpdateDateEnd = (date) => {
        setDatesDomainEnd(date)
    }

    const renderModalHintLoadingPanel = () => {
        if (!loading) {
            return (
                <StyledHintContainer>
                    <Icon name="hand point left outline"></Icon>
                    <Icon name="hand point up outline"></Icon>
                    <p>Escoge un departamento, provincia y distrito.</p>
                    <p>Opcionalmente, puedes escoger un indicador y un tipo de gráfico.</p>
                    <p>También puedes escoger un rango de fechas.</p>
                </StyledHintContainer>
            )
        } else {
            return (
                <StyledDimmer active>
                    <StyledLoader inverted>Cargando</StyledLoader>
                </StyledDimmer>
            )
        }
    }

    const renderModalVisualizer = () => {
        if (!dataPoints.length) {
          return null
        }

        const visualizerConfig = {
            dataPoints: dataPoints,
            metric: metric,
            chartType: chartType
        }
        return (
            <Visualizer
                config={visualizerConfig}
                zonesMap={zonesMap}
                title={title}
                updateDatesDomain={handleUpdateDatesDomain}
                dateStart={datesDomainStart}
                dateEnd={datesDomainEnd}
                edit></Visualizer>
        )
    }

    const handleAddVisualization = () => {
        handleAddVis({
            metric: metric,
            chartType: chartType,
            title: title,
            // dimensions: dimensions,
            // position: position,
            // add index on top
            // domain: domain,
            // dateRange: dateRange,
            dataPoints: dataPoints,
            dateStart: datesDomainStart,
            dateEnd: datesDomainEnd,
        })
        resetValues()
    }

    const renderDataPointListItem = (item) => {
        return (
            <ListItem>
                <List.Content floated='right'>
                    <ColorPickerTrigger icon
                        inverted
                        onClick={() => toggleColorPicker(item)}
                        style={{backgroundColor: item.color}}>
                        <Icon name="close"
                            style={{color: item.color}}></Icon>
                    </ColorPickerTrigger>
                    <ColorPickerContainer hidden={!item.showColorPicker}>
                        <ColorGrid columns={4}>
                            {colors.map(color => (
                                <ColorOption onClick={() => handleChangeColor(item, color)}>
                                    <ColorOptionInner style={{backgroundColor: color}} />
                                </ColorOption>
                            ))}
                        </ColorGrid>
                    </ColorPickerContainer>
                    <Button icon
                        inverted
                        onClick={() => handleRemove(item)}>
                        <Icon name="close"></Icon>
                    </Button>
                </List.Content>

                <List.Content style={{ paddingTop: '10px', color: item.color}} verticalAlign='middle'>
                    <StyledSpan>{item.departamento} / {item.provincia} / {item.distrito}</StyledSpan>
                </List.Content>
            </ListItem>
        )
    }

    return (
        <Modal inverted trigger={trigger}
            open={open}
            onClose={onClose}
            closeIcon>
            <StyledModalContent>
                <Grid stackable columns={2} celled="internally">
                    <Grid.Column>
                        <Selector
                            handleAdd={handleAdd}
                            showLoading={!!dataPoints.length && loading}
                            zonesMap={zonesMap}
                            showMetricOptions={!!dataPoints.length}
                            handleUpdateMetric={handleUpdateMetric}
                            handleUpdateChartType={handleUpdateChartType}
                            datesDomainStart={datesDomainStart}
                            datesDomainEnd={datesDomainEnd}
                            handleUpdateDateStart={handleUpdateDateStart}
                            handleUpdateDateEnd={handleUpdateDateEnd}
                            showAddButton={metric !== 'r_t' || dataPoints.length < 1} />
                    </Grid.Column>

                    <Grid.Column>
                        { !dataPoints.length ? renderModalHintLoadingPanel() : (
                        <List selection celled relaxed inverted divided verticalAlign='middle'>
                            {dataPoints.map(renderDataPointListItem)}
                        </List>
                        ) }
                    </Grid.Column>
                </Grid>

                <TitleInputContainer hidden={!dataPoints.length}>
                    <TitleInput
                        inverted
                        transparent
                        onChange={handleUpdateTitle}
                        placeholder={title} />
                </TitleInputContainer>

                {renderModalVisualizer()}
            </StyledModalContent>
            <StyledModalActions>
                <StyledAddVisButton inverted
                    disabled={!dataPoints.length}
                    onClick={handleAddVisualization}>
                    Agregar al tablero
                </StyledAddVisButton>
            </StyledModalActions>
        </Modal>
    )
}