import React, { useState, useEffect, useContext } from 'react'
import styled from 'styled-components'
import firebaseDatabase from 'firebase/database'
import shortid from 'shortid'
import queryString from 'query-string'
import { Grid, Button, Modal, List, Segment, Loader, Dimmer } from 'semantic-ui-react'

import { Visualizer } from '../Visualizer'
import { buildConfigJson, processDataPointResponse } from '../../utils'
import { colors } from './constants'
import { FirebaseContext } from '../../utils/firebase'
import { AddVisModal } from '../AddVisModal/AddVisModal';
import {
    StyledModalContent,
    StyledModalActions,
} from '../AddVisModal/AddVisModal'

const BigButton = styled(Button)`
    width: 50%;
    height: 40%;
    position: absolute;
    top: 30%;
    left: 30%;

    i {
        line-height: 60px;
        font-size: 75px;
    }
`

const CellDiv = styled.div`
    width: 100%;
    height: 356px;
    padding: 10px;
`

const StyledModalHeader = styled(Modal.Header)`
    background-color: #1f1c1c !important;
    color: #fafafa !important;
    font-family: 'Bitter',serif !important;
`

const StyledLoader = styled(Loader)`
  font-family: 'Bitter', serif;
`

const StyledDimmer = styled(Dimmer)`
  background-color: #1f1c1c !important;
`

export const Dashboard = ({ zonesMap }) => {
    const [config, setConfig] = useState([])
    const [showPlusButton, setShowPlusButton] = useState(false)

    const [modalOpen, setModalOpen] = useState(false)
    const [nextIndex, setNextIndex] = useState(0)

    const [showDownloadModal, setShowDownloadModal] = useState(false)
    const [downloadModalData, setDownloadModalData] = useState([])

    const firebase = useContext(FirebaseContext)

    useEffect(e => {
        let data
        if (!zonesMap) return

        setShowPlusButton(true)

        if (window.location && window.location.search) {
            const parsedQueryString = queryString.parse(location.search)
            if (parsedQueryString.stateId) {
                firebase.database()
                    .ref(`/${parsedQueryString.stateId}`).once('value').then(snapshot => {
                        const savedConfig = JSON.parse(snapshot.val())

                        if (savedConfig) {
                            loadFromConfig(savedConfig)
                        }
                    })
                return
            }

            if (parsedQueryString.state) {
                data = decodeURIComponent(parsedQueryString.state)

                const visualizations = data.split(';')
                const newConfigs = visualizations.map((vis, i) => {
                    const metric = vis.split('-')[1]
                    const chartType = vis.split('-')[2]
                    const series = vis.split('-')[0].split('|')

                    const names = series.map(tuple => {
                        return {
                            departamento: decodeURIComponent(tuple.split('@')[0]),
                            provincia: decodeURIComponent(tuple.split('@')[1]),
                            distrito: decodeURIComponent(tuple.split('@')[2])
                        }
                    })

                    const newConfig = {
                        dataPoints: names.map((name) => {
                            return {
                                distrito: name.distrito,
                                provincia: name.provincia,
                                departamento: name.departamento,
                                color: colors[Math.floor(Math.random() * colors.length)],
                            }
                        }),
                        metric: metric,
                        chartType: chartType,
                        visKey: shortid.generate(),
                    }

                    return newConfig
                })

                loadFromConfig(newConfigs)
                const stateId = shortid.generate()
                const newUrl = window.location.protocol + "//" +
                    window.location.host + window.location.pathname +
                    `?stateId=${stateId}`
                window.history.pushState({path:newUrl}, '', newUrl)
                firebase.database()
                    .ref(`/${stateId}`).set(JSON.stringify(newConfigs))
            }
        }
    }, [zonesMap])

    const loadFromConfig = (savedConfig) => {
        setConfig(savedConfig)
        setNextIndex(savedConfig.length)
    }

    const openModal = (e) => {
        setModalOpen(true)
    }

    const handleModalClose = (e) => {
        setModalOpen(false)
    }

    const handleAddVis = (visualization) => {
        const newVis = {
            ...visualization,
            index: nextIndex,
            key: `vis-${nextIndex}`,
            visKey: shortid.generate(),
        }

        setConfig(config => {
            return [...config, newVis]
        })

        setModalOpen(false)
        setNextIndex(nextIndex => nextIndex + 1)

        // Save to Firebase
        const parsedQueryString = queryString.parse(location.search)
        let stateId = parsedQueryString.stateId
        if (!stateId) {
            stateId = shortid.generate()
            const newUrl = window.location.protocol + "//" +
                window.location.host + window.location.pathname +
                `?stateId=${stateId}`
            window.history.pushState({path:newUrl}, '', newUrl)
        }
        firebase.database()
           .ref(`/${stateId}`).set(
               JSON.stringify(buildConfigJson([...config, newVis]))
            )
    }

    const handleRemoveVis = (visKey) => {
        const newConfig = config.filter(series => {
            return series.visKey !== visKey
        })
        setConfig(newConfig)

        // Save to Firebase
        const parsedQueryString = queryString.parse(location.search)
        let stateId = parsedQueryString.stateId
        if (!stateId) {
            stateId = shortid.generate()
            const newUrl = window.location.protocol + "//" +
                window.location.host + window.location.pathname +
                `?stateId=${stateId}`
            window.history.pushState({path:newUrl}, '', newUrl)
        }
        firebase.database()
           .ref(`/${stateId}`).set(
               JSON.stringify(buildConfigJson(newConfig))
            )
        setModalOpen(false)
    }

    const handleShowDownloadModal = (data) => {
        setShowDownloadModal(true)
        setDownloadModalData(data)
    }

    const renderVisualization = (vis) => {
        const visualizerConfig = {
            dataPoints: vis.dataPoints,
            metric: vis.metric,
            chartType: vis.chartType,
            title: vis.title,
        }
        return (
            <Grid.Column>
                <Visualizer
                    config={visualizerConfig}
                    zonesMap={zonesMap}
                    title={vis.title}
                    removeVis={handleRemoveVis}
                    dateStart={vis.dateStart}
                    dateEnd={vis.dateEnd}
                    visKey={vis.visKey}
                    onShowDownloadModal={handleShowDownloadModal} />
            </Grid.Column>
        )
    }

    const renderDownloadItem = (item) => {
        const { departamento, provincia, name } = item
        let jsonLink, csvLink, label

        if (provincia && name) {
            csvLink = `https://covidatos-regiones.s3-sa-east-1.amazonaws.com/${departamento}/${provincia}/${name}.csv`
            jsonLink = `https://covidatos-regiones.s3-sa-east-1.amazonaws.com/${departamento}/${provincia}/${name}.json`
            label = `${departamento} / ${provincia} / ${name}`
        } else if (provincia) {
            csvLink = `https://covidatos-regiones.s3-sa-east-1.amazonaws.com/${departamento}/${provincia}.csv`
            jsonLink = `https://covidatos-regiones.s3-sa-east-1.amazonaws.com/${departamento}/${provincia}.json`
            label = `${departamento} / ${provincia}`
        } else {
            csvLink = `https://covidatos-regiones.s3-sa-east-1.amazonaws.com/${departamento}.csv`
            jsonLink = `https://covidatos-regiones.s3-sa-east-1.amazonaws.com/${departamento}.json`
            label = `${departamento}`
        }

        return (
            <List.Item>
                <List.Content>
                    <a href={csvLink}>
                        <Button inverted size='mini'>
                            .CSV
                        </Button>
                    </a>
                    <a href={jsonLink}>
                        <Button inverted size='mini'>
                            .JSON
                        </Button>
                    </a>
                    {label}
                </List.Content>
            </List.Item>
        )
    }

    return (
        <>
            <Grid stackable columns={3}>
                {config.map(vis => (
                    renderVisualization(vis)
                ))}
                {showPlusButton ? (
                        <Grid.Column>
                            <CellDiv>
                                <AddVisModal
                                    zonesMap={zonesMap}
                                    onClickOpenModal={openModal}
                                    trigger={<BigButton inverted onClick={openModal} icon="plus" />}
                                    open={modalOpen}
                                    onClose={handleModalClose}

                                    handleAddVis={handleAddVis}
                                ></AddVisModal>
                            </CellDiv>
                        </Grid.Column>
                    ) : (
                        <StyledDimmer active>
                            <StyledLoader inverted>Cargando</StyledLoader>
                        </StyledDimmer>
                    )
                }
            </Grid>
            <Modal size='tiny' onClose={() => {setShowDownloadModal(false)}}
                closeIcon
                open={showDownloadModal} >
                <StyledModalHeader>Descargar datos</StyledModalHeader>
                <StyledModalContent>
                    {/* <p>Are you sure you want to delete your account</p> */}
                    <Segment inverted>
                        <List divided inverted relaxed>
                            {downloadModalData.map(renderDownloadItem)}
                        </List>
                    </Segment>
                </StyledModalContent>
                <StyledModalActions>
                    <Button inverted onClick={() => {setShowDownloadModal(false)}}>Volver</Button>
                </StyledModalActions>
            </Modal>
        </>
    )
}