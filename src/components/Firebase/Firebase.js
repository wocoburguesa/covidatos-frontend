import app from 'firebase/app';

// const prodConfig = {
//     apiKey: process.env.REACT_APP_PROD_API_KEY,
//     authDomain: process.env.REACT_APP_PROD_AUTH_DOMAIN,
//     databaseURL: process.env.REACT_APP_PROD_DATABASE_URL,
//     projectId: process.env.REACT_APP_PROD_PROJECT_ID,
//     storageBucket: process.env.REACT_APP_PROD_STORAGE_BUCKET,
//     messagingSenderId: process.env.REACT_APP_PROD_MESSAGING_SENDER_ID,
// };

// const devConfig = {
//     apiKey: process.env.REACT_APP_DEV_API_KEY,
//     authDomain: process.env.REACT_APP_DEV_AUTH_DOMAIN,
//     databaseURL: process.env.REACT_APP_DEV_DATABASE_URL,
//     projectId: process.env.REACT_APP_DEV_PROJECT_ID,
//     storageBucket: process.env.REACT_APP_DEV_STORAGE_BUCKET,
//     messagingSenderId: process.env.REACT_APP_DEV_MESSAGING_SENDER_ID,
// };

const config = {
    apiKey: "AIzaSyAWnPCFoqx_FIccvaSuJZ9eH4W72dMZcWE",
    authDomain: "covidatos-b4788.firebaseapp.com",
    databaseURL: "https://covidatos-b4788.firebaseio.com",
    projectId: "covidatos-b4788",
    storageBucket: "covidatos-b4788.appspot.com",
    messagingSenderId: "749009232866",
    appId: "1:749009232866:web:874ed127d4a0217c1acfbb",
    measurementId: "G-V28Z3QNJD7"
}

// const config =
//   process.env.NODE_ENV === 'production' ? prodConfig : devConfig;

class Firebase {
  constructor() {
    app.initializeApp(config);

    this.db = app.database()

    session = (id) => this.db.ref('session')
  }
}

export default Firebase;