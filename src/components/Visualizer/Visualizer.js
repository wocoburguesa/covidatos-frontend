import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import {
    LineChart,
    BarChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
    Bar,
    AreaChart,
    Area,
    ComposedChart
} from 'recharts'

import { Menu, Icon, Button, Loader, Dimmer } from 'semantic-ui-react'

import { dataPointsService } from '../../services'
import { processDataPointResponse } from '../../utils'
import { colors } from '../AddVisModal/constants'

const months = {
    '03': 'marzo',
    '04': 'abril',
    '05': 'mayo',
    '06': "junio",
    '07': 'julio',
    '08': 'agosto'
}

const StyledPopup = styled.div`
    width: 300px;
    background-color: #1f1c1c;
    padding: 10px 20px;
    border: 1px solid #fafafa;
    border-radius: 3px;
    font-family: 'Bitter', serif;

    div {
        border-bottom: 1px solid #fafafa;
        margin-bottom: 5px;
    }

    div:last-child {
        border-bottom: none;
        margin-bottom: 0;
    }

    span {
        font-family: 'Bitter', serif;
        color: #fafafa;
        float: right
    }

    p {
        font-family: 'Bitter', serif;
    }
`

const StyledTitle = styled.h1`
    font-size: 16px;
    text-align: center;
    color: #f9f9f9;
    font-family: 'Bitter', serif;
`

const CellOptionsButton = styled(Button)``

const MenuContainer = styled.div `
    position: absolute;
    z-index: 99;
`

const StyledLoader = styled(Loader)`
  font-family: 'Bitter', serif;
`

const StyledDimmer = styled(Dimmer)`
  background-color: #1f1c1c !important;
`

export const CustomTooltip = ({ type, label, payload }) => {
    const dateParts = label ? label.split('-') : []
    let formattedLabel = ''

    if (dateParts.length) {
        formattedLabel = `${label.split('-')[2]} de ${months[label.split('-')[1]]}`
    }

    return (
        <StyledPopup>
            <p>{formattedLabel}</p>
            {   payload.map(row => {
                    const suffix = row.dataKey.indexOf('pct') !== -1 ? ' %' : ''
                    const value = (row.dataKey.indexOf('pct') !== -1 || row.dataKey.indexOf('100k') !== -1)
                        ? row.value.toFixed(2)
                        : row.value

                    if (row.dataKey === 'r_range') {
                        const firstValue = row.value[0] ? row.value[0].toFixed(2) : null
                        const secondValue = row.value[1] ? row.value[1].toFixed(2) : null
                        const rangeValue = (firstValue && secondValue) ? `${firstValue} - ${secondValue}` : null
                        return <div>
                            <label style={{ color: row.stroke }}>{row.name}</label>
                            <span>{ rangeValue }</span>
                        </div>
                    } else if (row.dataKey === 'r_t') {
                        return <div>
                            <label style={{ color: row.stroke }}>{row.name}</label>
                            <span>{ row.value.toFixed(2) }</span>
                        </div>
                    } else {
                        return <div>
                            <label style={{ color: row.stroke }}>{row.name}</label>
                            <span>{ value }{ suffix }</span>
                        </div>
                    }

                })
            }
        </StyledPopup>
    )
}

export const Visualizer = ({
    config,
    zonesMap,
    removeVis,
    edit,
    title,
    updateDatesDomain = () => {},
    dateStart,
    dateEnd,
    visKey,
    onShowDownloadModal = () => {},
}) => {
    const [showMenu, setShowMenu] = useState(false)
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(e => {
        // Removing a data point
        if (config.dataPoints.length < data.length) {
            setData(data.filter(existingDataPoint => {
                const found = config.dataPoints.filter(
                    newDataPoint => newDataPoint.distrito === existingDataPoint.name)
                return !!found.length
            }))
            return
        }

        setLoading(true)
        dataPointsService.getBatchDataPoints(
            config.dataPoints.map(dataPoint => {
                return {
                    departamento: dataPoint.departamento,
                    provincia: dataPoint.provincia,
                    distrito: dataPoint.distrito
                }
            }),
            (responses) => {
                const toProcess = responses.map((response, i) => {
                    const { departamento, provincia, distrito, color } = config.dataPoints[i]
                    const key = `dataPoint-${[departamento, provincia, distrito].join('-')}`

                    return {
                        name: distrito,
                        provincia: provincia,
                        departamento: departamento,
                        data: response.data,
                        color: color,
                        dataPointKey: key
                    }
                })

                let processed = []
                if (!dateStart || !dateEnd) {
                    processed = processDataPointResponse(
                        toProcess,
                        null,
                        zonesMap
                    )
                    updateDatesDomain(
                        processed[0].data[0].date, // first date
                        processed[0].data[processed[0].data.length - 1].date // last date
                    )
                } else {
                    processed = processDataPointResponse(
                        toProcess,
                        null,
                        zonesMap,
                        {
                            minDate: new Date(dateStart),
                            maxDate: new Date(dateEnd)
                        }
                    )
                }
                setData(processed)
                setLoading(false)
            }
        )
    }, [config])

    const renderColorfulLegendText = (value, entry) => {
        const { color } = entry;
        return <span style={{ color }}>{value}</span>;
    }

    const toggleShowMenu = (e) => {
        setShowMenu(showMenu => !showMenu)
    }

    const handleRemoveVis = () => {
        removeVis(visKey)
    }

    const renderLoadingOverlay = () => {
        return loading ? (
            <StyledDimmer active>
                <StyledLoader inverted>Cargando</StyledLoader>
            </StyledDimmer>
        ) : null
    }

    const renderChart = () => {
        if (!data.length) return <></>

        if (config.metric === 'r_t') {
            const series = data[0]
            series.data = series.data.map(row => {
                return {
                    ...row,
                    r_range: [row['r_low_90'], row['r_high_90']]
                }
            })
            return (
                <ComposedChart data={series.data}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                    <XAxis dataKey="date" allowDuplicatedCategory={false} type={"category"}/>
                    <YAxis/>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <Tooltip content={<CustomTooltip/>}/>
                    <Legend formatter={renderColorfulLegendText}/>
                    <Line
                        dot={true}
                        connectNulls
                        type="monotone"
                        dataKey={'r_t'}
                        name={`R de ${series.name}`}
                        strokeWidth={2}
                        // key={series.key}
                        stroke={series.color}
                    />
                    <Area
                        connectNulls
                        dataKey={'r_range'}
                        name={'Rango posible de R'}
                        // key={series.key}
                        stroke={series.color}
                        fill={series.color}
                    />
                </ComposedChart>
            )
        }

        if (config.chartType === 'line') {
            return (
                <LineChart data={data}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                    <XAxis dataKey="date" allowDuplicatedCategory={false} type={"category"}/>
                    <YAxis/>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <Tooltip content={<CustomTooltip/>}/>
                    <Legend formatter={renderColorfulLegendText}/>
                    {data.map(series => (
                        <Line
                            dot={false}
                            connectNulls
                            type="monotone"
                            dataKey={config.metric}
                            data={series.data}
                            name={series.name}
                            key={series.key}
                            stroke={series.color}
                        />
                    ))}
                </LineChart>
            )
        } else if (config.chartType === 'bar') {
            return (
                <BarChart data={data}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                    <XAxis dataKey="date" allowDuplicatedCategory={false} type={"category"}/>
                    <YAxis/>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <Tooltip content={<CustomTooltip/>}/>
                    <Legend formatter={renderColorfulLegendText}/>
                    {data.map(series => (
                        <Bar
                            dataKey={config.metric}
                            data={series.data}
                            name={series.name}
                            key={series.key}
                            stroke={series.color}
                            fill={series.color}
                        />
                    ))}
                </BarChart>
            )
        } else if (config.chartType === 'area') {
            return (
                <AreaChart data={data}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                    <XAxis dataKey="date" allowDuplicatedCategory={false} type={"category"}/>
                    <YAxis/>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <Tooltip content={<CustomTooltip/>}/>
                    <Legend formatter={renderColorfulLegendText}/>
                    {data.map(series => (
                        <Area
                            connectNulls
                            dataKey={config.metric}
                            data={series.data}
                            name={series.name}
                            key={series.key}
                            stroke={series.color}
                            fill={series.color}
                        />
                    ))}
                </AreaChart>
            )
        }
    }

    const handleShowDownloadModal = () => {
        onShowDownloadModal(data)
    }

    return (
        <>
            { !edit && <CellOptionsButton inverted icon="options"
                    onClick={toggleShowMenu}
            ></CellOptionsButton> }

            <MenuContainer hidden={!showMenu}>
                <Menu icon vertical>
                    {/* <Menu.Item
                        >
                        <Icon name='edit' />
                    </Menu.Item>*/}

                    <Menu.Item
                        onClick={handleShowDownloadModal}
                        >
                        <Icon name='download' />
                    </Menu.Item>

                    <Menu.Item
                        onClick={handleRemoveVis}
                        >
                        <Icon name='trash' />
                    </Menu.Item>
                </Menu>
            </MenuContainer>

            { !edit && <StyledTitle>{title}</StyledTitle>}

            {renderLoadingOverlay()}

            <ResponsiveContainer  width="100%" height={356}>
                {renderChart()}
            </ResponsiveContainer>
        </>
    )
}