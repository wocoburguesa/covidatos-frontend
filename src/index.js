import React from 'react'
import { render } from 'react-dom'

import FirebaseProvider from './utils/firebase'
// import Firebase, { FirebaseContext } from './components/Firebase';
import App from './App'

render(
    <FirebaseProvider>
        <App />
    </FirebaseProvider>,
    document.getElementById('react-app-container')
)