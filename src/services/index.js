import axios from 'axios'

export const dataPointsService = {
    getDataPointsForDistrito: getDataPointsForDistrito,
    getBatchDataPoints: getBatchDataPoints,
}

function getDataPointsForDistrito(
    departamento,
    provincia,
    distrito,
    callback
) {
    const url = `https://covidatos-regiones.s3-sa-east-1.amazonaws.com/${departamento}/${provincia}/${distrito}.json`
    axios.get(url).then(res => callback(res))
}

function getBatchDataPoints(dataPoints, callback) {
    const requests = dataPoints.map(dataPoint => {
        const {
            departamento,
            provincia,
            distrito
        } = dataPoint
        return axios.get(`https://covidatos-regiones.s3-sa-east-1.amazonaws.com/${departamento}/${provincia}/${distrito}.json`)
    })

    axios.all(requests).then(axios.spread((...responses) => {callback(responses)}))
}