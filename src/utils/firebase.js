import React, { createContext } from 'react'
import app from 'firebase/app'

const FirebaseContext = createContext(null);

// export const withFirebase = Component => props => (
//     <FirebaseContext.Consumer>
//       {firebase => <Component {...props} firebase={firebase} />}
//     </FirebaseContext.Consumer>
//   );

export { FirebaseContext }

export default ({ children }) => {
    if (!app.apps.length) {
         app.initializeApp({
            apiKey: "aqui pones tus datos de firebase",
            authDomain: "aqui pones tus datos de firebase",
            databaseURL: "aqui pones tus datos de firebase",
            projectId: "aqui pones tus datos de firebase",
            storageBucket: "aqui pones tus datos de firebase",
            messagingSenderId: "aqui pones tus datos de firebase",
            appId: "aqui pones tus datos de firebase",
            measurementId: "aqui pones tus datos de firebase"
         })
    }
    return (
        <FirebaseContext.Provider value={ app }>
            { children }
        </FirebaseContext.Provider>
    )
  }