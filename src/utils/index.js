import { metricOptions } from "../components/Selector/constants";

const buildDateRange = (startDate, stopDate) => {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(new Date (currentDate));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;

}

export const normalizeDataPoints = (data, newData, inputDateRange = null) => {
    let minDate, maxDate

    if (!inputDateRange) {
        if (newData) {
            const newDataDates = newData.data.map(row => new Date(row.date))
            newDataDates.forEach(date => {
                if (!minDate) {
                    minDate = date
                } else {
                    if (date < minDate) {
                        minDate = date
                    }
                }

                if (!maxDate) {
                    maxDate = date
                } else {
                    if (date > maxDate) {
                        maxDate = date
                    }
                }
            })
        }

        data.forEach((distrito) => {
            const dates = distrito.data.map(row => new Date(row.date))
            dates.forEach(date => {
                if (!minDate) {
                    minDate = date
                } else {
                    if (date < minDate) {
                        minDate = date
                    }
                }

                if (!maxDate) {
                    maxDate = date
                } else {
                    if (date > maxDate) {
                        maxDate = date
                    }
                }
            })
        })
    } else {
        minDate = inputDateRange.minDate
        maxDate = inputDateRange.maxDate
    }

    const result = newData ? [...data, newData] : data

    const dateRange = buildDateRange(minDate, maxDate)

    result.forEach(distrito => {
        const bufferRows = distrito.data

        distrito.data = dateRange.map(date => {
            date.setHours(date.getHours() + 5)
            const stringDate = date.yyyymmdd()
            const filter = bufferRows.filter(row => row.date === stringDate)

            if (filter.length) {
                return filter[0]
            } else {
                return {
                    date: stringDate,
                    daily_value: null,
                    cumulative_value: null,
                    growth_rate: null,
                    acceleration: null
                }
            }
        })
    })

    return result
}

export const processDataPointResponse = (data, newData, zonesMap, dateRange = null) => {
    const normalized = normalizeDataPoints(
        data,
        newData,
        dateRange
    )

    const withPopulation = addPopulationData(zonesMap, normalized)

    return withPopulation
}

export const getPopulation0To5Years = (populationData) => {
    return populationData.poblacion_0 + populationData.poblacion_0_5_meses +
        populationData.poblacion_1 + populationData.poblacion_2 + populationData.poblacion_3 +
        populationData.poblacion_4 + populationData.poblacion_5
}

export const getPopulation6To18Years = (populationData) => {
    let i = 6, end = 19
    let sum = 0

    for (; i < end; i++) {
        sum += populationData[`poblacion_${i}`]
    }

    return sum
}

export const getPopulation19To24Years = (populationData) => {
    return populationData.poblacion_19 + populationData.poblacion_20_24
}

export const getPopulation25To39Years = (populationData) => {
    return populationData.poblacion_25_29 + populationData.poblacion_30_34 +
        populationData.poblacion_35_39
}

export const getPopulation40To64Years = (populationData) => {
    return populationData.poblacion_40_44 + populationData.poblacion_45_49 +
        populationData.poblacion_50_54 + populationData.poblacion_55_59 +
        populationData.poblacion_60_64
}

export const getPopulationOver65Years = (populationData) => {
    return populationData.poblacion_65_69 + populationData.poblacion_70_74 +
        populationData.poblacion_75_79 + populationData.poblacion_80_mas
}

export const getTotalPopulation = (populationData) => {
    return populationData.poblacion_total
}

export const addPopulationData = (zonesMap, config) => {
    config.forEach(dataPoint => {
        const populationData = zonesMap[dataPoint.departamento][dataPoint.provincia][dataPoint.name]
        Object.keys(populationData).forEach(key => {
            populationData[key] = Number(populationData[key])

        })

        dataPoint.data.forEach(row => {
            row.cumulative_pct_total = (row.cumulative_value / getTotalPopulation(populationData)) * 100 || null
            // row.cumulative_pct_0_5 = (row.cumulative_0_5 / getPopulation0To5Years(populationData)) * 100 || null
            // row.cumulative_pct_6_18 = (row.cumulative_6_18 / getPopulation6To18Years(populationData)) * 100 || null
            // row.cumulative_pct_19_24 = (row.cumulative_19_24 / getPopulation19To24Years(populationData)) * 100 || null
            // row.cumulative_pct_25_39 = (row.cumulative_25_39 / getPopulation25To39Years(populationData) * 100) || null
            // row.cumulative_pct_40_64 = (row.cumulative_40_64 / getPopulation40To64Years(populationData)) * 100 || null
            // row.cumulative_pct_over_65 = (row.cumulative_over_65 / getPopulationOver65Years(populationData)) * 100 || null
            row.cumulative_pct_0_5 = (row.cumulative_0_5 / getTotalPopulation(populationData)) * 100 || null
            row.cumulative_pct_6_18 = (row.cumulative_6_18 / getTotalPopulation(populationData)) * 100 || null
            row.cumulative_pct_19_24 = (row.cumulative_19_24 / getTotalPopulation(populationData)) * 100 || null
            row.cumulative_pct_25_39 = (row.cumulative_25_39 / getTotalPopulation(populationData) * 100) || null
            row.cumulative_pct_40_64 = (row.cumulative_40_64 / getTotalPopulation(populationData)) * 100 || null
            row.cumulative_pct_over_65 = (row.cumulative_over_65 / getTotalPopulation(populationData)) * 100 || null

            row.cumulative_100k_total = (row.cumulative_pct_total * 1000) || null
            row.cumulative_100k_0_5 = (row.cumulative_pct_0_5 * 1000) || null
            row.cumulative_100k_6_18 = (row.cumulative_pct_6_18 * 1000) || null
            row.cumulative_100k_19_24 = (row.cumulative_pct_19_24 * 1000) || null
            row.cumulative_100k_25_39 = (row.cumulative_pct_25_39 * 1000) || null
            row.cumulative_100k_40_64 = (row.cumulative_pct_40_64 * 1000) || null
            row.cumulative_100k_over_65 = (row.cumulative_pct_over_65 * 1000) || null
        })
    })

    return config
}

export const buildConfigJson = (config) => {
    return config.map(vis => {
        return {
            metric: vis.metric,
            title: vis.title,
            dimensions: vis.dimensions,
            position: vis.position,
            index: vis.index,
            domain: vis.domain,
            dateStart: vis.dateStart,
            dateEnd: vis.dateEnd,
            chartType: vis.chartType,
            visKey: vis.visKey,
            dataPoints: vis.dataPoints.map(dataPoint => {
                return {
                    departamento: dataPoint.departamento,
                    provincia: dataPoint.provincia,
                    distrito: dataPoint.distrito,
                    color: dataPoint.color,
                    chartType: dataPoint.chartType
                }
            })
        }
    })
}

export const getMetricTextName = (metric, metricOptions) => {
    const filtered = metricOptions.filter(option => option.key === metric)

    if (!filtered.length) return ''

    return filtered[0].text
}